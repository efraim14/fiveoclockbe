<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkTblXItemVoucher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TblXItemVoucher', function (Blueprint $table) {
            $table->foreign('ItemVariantId')->references('ItemVariantId')->on('TblMItemVariant');
            $table->foreign('VoucherId')->references('VoucherId')->on('TblMVoucher');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TblXItemVoucher', function (Blueprint $table) {
            $table->dropForeign('tblxitemvoucher_itemvariantid_foreign');
            $table->dropForeign('tblxitemvoucher_voucherid_foreign');
        });
    }
}
