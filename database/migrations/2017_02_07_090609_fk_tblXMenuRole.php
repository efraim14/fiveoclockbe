<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkTblXMenuRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TblXMenuRole', function (Blueprint $table) {  
            $table->foreign('RoleId')->references('RoleId')->on('TblMRole');
            $table->foreign('MenuId')->references('MenuId')->on('TblMMenu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TblXMenuRole', function (Blueprint $table) {
            $table->dropForeign('tblxmenurole_roleid_foreign');
            $table->dropForeign('tblxmenurole_menuid_foreign');
        });
    }
}
