<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblTOrderLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TblTOrderLog', function (Blueprint $table) {
            $table->bigIncrements('OrderLogId');
            $table->bigInteger('OrderId')->unsigned();
            $table->string('Status');
            $table->timestamp('TimeStamp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TblTOrderLog');
    }
}
