<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblTOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TblTOrder', function (Blueprint $table) {
            $table->bigIncrements('OrderId');
            $table->bigInteger('ProfileId')->unsigned();
            $table->bigInteger('VoucherId')->nullable()->unsigned();
            $table->bigInteger('CourierId')->nullable()->unsigned();
            $table->string('PaymentMethod');
            $table->string('NoResi')->nullable();
            $table->string('Notes')->nullable();
            $table->string('Invoice')->nullable();
            $table->timestamp('OrderDate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TblTOrder');
    }
}
