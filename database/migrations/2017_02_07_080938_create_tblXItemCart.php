<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblXItemCart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TblXItemCart', function (Blueprint $table) {
            $table->bigIncrements('ItemCartId');
            $table->bigInteger('ItemVariantId')->unsigned();
            $table->bigInteger('CartId')->unsigned();
            $table->integer('Quantity');
            $table->string('Notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TblXItemCart');
    }
}
