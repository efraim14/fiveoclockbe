<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblXItemOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TblXItemOrder', function (Blueprint $table) {
            $table->bigIncrements('ItemOrderId');
            $table->bigInteger('ItemVariantId')->unsigned();
            $table->bigInteger('OrderId')->unsigned();
            $table->integer('Quantity');
            $table->string('Notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TblXItemOrder');
    }
}
