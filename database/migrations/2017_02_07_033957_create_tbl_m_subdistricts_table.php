<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblMSubdistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TblMSubdistrict', function (Blueprint $table) {
            $table->bigIncrements('SubdistrictId');
            $table->bigInteger('CityId')->unsigned();
            $table->string('SubdistrictName');
            $table->integer('ZipCode');
            $table->boolean('IsActive')->default(1);
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TblMSubdistrict');
    }
}
