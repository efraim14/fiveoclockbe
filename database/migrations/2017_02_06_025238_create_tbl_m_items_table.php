<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblMItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TblMItem', function (Blueprint $table) {
            $table->bigIncrements('ItemId');
            $table->bigInteger('BrandId')->unsigned();
            $table->string('ItemName');
            $table->bigInteger('BasePrice');
            $table->bigInteger('ResellerPrice');
            $table->bigInteger('Price');
            $table->integer('Weight');
            $table->bigInteger('Discount');
            $table->string('Notes')->nullable();
            $table->boolean('IsActive')->default(1);
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TblMItem');
    }
}
