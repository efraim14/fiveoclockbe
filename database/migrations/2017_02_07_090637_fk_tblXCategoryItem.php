<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkTblXCategoryItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TblXCategoryItem', function (Blueprint $table) {
            $table->foreign('ItemId')->references('ItemId')->on('TblMItem');
            $table->foreign('CategoryId')->references('CategoryId')->on('TblMCategory');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TblXCategoryItem', function (Blueprint $table) {
            $table->dropForeign('tblxcategoryitem_itemid_foreign');
            $table->dropForeign('tblxcategoryitem_categoryid_foreign');
        });
    }
}
