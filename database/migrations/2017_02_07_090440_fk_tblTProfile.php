<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkTblTProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TblTProfile', function (Blueprint $table) {
            $table->foreign('UserId')->references('UserId')->on('TblMUser');
            $table->foreign('SubdistrictId')->references('SubdistrictId')->on('TblMSubdistrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TblTProfile', function (Blueprint $table) {
            $table->dropForeign('tbltprofile_userid_foreign');
            $table->dropForeign('tbltprofile_subdistrictid_foreign');
        });
    }
}
