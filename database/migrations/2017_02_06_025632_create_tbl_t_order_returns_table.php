<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblTOrderReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TblTOrderReturn', function (Blueprint $table) {
            $table->bigIncrements('OrderReturnId');
            $table->bigInteger('OrderId')->unsigned();
            $table->timestamp('TimeStamp');
            $table->string('Notes');
            $table->boolean('IsActive')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_t_order_returns');
    }
}
