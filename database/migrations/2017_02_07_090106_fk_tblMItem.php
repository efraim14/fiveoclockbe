<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkTblMItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TblMItem', function (Blueprint $table) {
            $table->foreign('BrandId')->references('BrandId')->on('TblMBrand');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TblMItem', function (Blueprint $table) {
            $table->dropForeign('tblmitem_brandid_foreign');
        });
    }
}
