<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkTblTToken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TblTToken', function (Blueprint $table) {
            $table->foreign('UserId')->references('UserId')->on('TblMUser');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TblTToken', function (Blueprint $table) {
            $table->dropForeign('tblttoken_userid_foreign');
        });
    }
}
