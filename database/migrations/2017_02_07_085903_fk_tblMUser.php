<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkTblMUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TblMUser', function (Blueprint $table) {
            $table->foreign('RoleId')->references('RoleId')->on('TblMRole');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TblMUser', function (Blueprint $table) {
            $table->dropForeign('tblmuser_roleid_foreign');
        });
    }
}
