<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkTblXItemCart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TblXItemCart', function (Blueprint $table) {
            $table->foreign('ItemVariantId')->references('ItemVariantId')->on('TblMItemVariant');
            $table->foreign('CartId')->references('CartId')->on('TblTCart');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TblXItemCart', function (Blueprint $table) {
            $table->dropForeign('tblxitemcart_itemvariantid_foreign');
            $table->dropForeign('tblxitemcart_cartid_foreign');
        });
    }
}
