<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblMVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TblMVoucher', function (Blueprint $table) {
            $table->bigIncrements('VoucherId');
            $table->string('VoucherName');
            $table->integer('Limit');
            $table->integer('Stock');
            $table->timestamp('ExpirationDate');
            $table->string('Notes')->nullable();
            $table->boolean('IsActive')->default(1);
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TblMVoucher');
    }
}
