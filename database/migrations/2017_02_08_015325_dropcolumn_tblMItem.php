<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropcolumnTblMItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TblMItem', function (Blueprint $table) {
            $table->dropColumn(['BasePrice', 'ResellerPrice', 'Price', 'Discount', 'Weight', 'Notes']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TblMItem', function (Blueprint $table) {
            $table->bigInteger('BasePrice');
            $table->bigInteger('ResellerPrice');
            $table->bigInteger('Price');
            $table->integer('Weight');
            $table->bigInteger('Discount');
            $table->string('Notes')->nullable();
        });
    }
}
