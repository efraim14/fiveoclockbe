<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblXItemOrderReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TblXItemOrderReturn', function (Blueprint $table) {
            $table->bigIncrements('ItemOrderReturnId');
            $table->bigInteger('ItemVariantId')->unsigned();
            $table->bigInteger('OrderReturnId')->unsigned();
            $table->string('Description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TblXItemOrderReturn');
    }
}
