<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkTblMCity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TblMCity', function (Blueprint $table) {
            $table->foreign('ProvinceId')->references('ProvinceId')->on('TblMProvince');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TblMCity', function (Blueprint $table) {
            $table->dropForeign('tblmcity_provinceid_foreign');
        });
    }
}
