<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblXItemVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TblXItemVoucher', function (Blueprint $table) {
            $table->bigIncrements('ItemVoucherId');
            $table->bigInteger('ItemVariantId')->unsigned();
            $table->bigInteger('VoucherId')->unsigned();
            $table->boolean('IsActive')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TblXItemVoucher');
    }
}
