<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblTTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TblTToken', function (Blueprint $table) {
            $table->bigIncrements('TokenId');
            $table->bigInteger('UserId')->unsigned();
            $table->string('Token');
            $table->timestamp('ExpiredTimeStamp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TblTToken');
    }
}
