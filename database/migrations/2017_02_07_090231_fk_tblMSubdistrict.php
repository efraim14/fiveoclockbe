<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkTblMSubdistrict extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TblMSubdistrict', function (Blueprint $table) {
            $table->foreign('CityId')->references('CityId')->on('TblMCity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TblMSubdistrict', function (Blueprint $table) {
            $table->dropForeign('tblmsubdistrict_cityid_foreign');
        });
    }
}
