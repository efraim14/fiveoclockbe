<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkTblMItemVariant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TblMItemVariant', function (Blueprint $table) {
            $table->foreign('ItemId')->references('ItemId')->on('TblMItem');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TblMItemVariant', function (Blueprint $table) {
            $table->dropForeign('tblmitemvariant_itemid_foreign');
        });
    }
}
