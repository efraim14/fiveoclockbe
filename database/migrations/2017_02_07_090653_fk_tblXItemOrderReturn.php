<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkTblXItemOrderReturn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TblXItemOrderReturn', function (Blueprint $table) {
            $table->foreign('ItemVariantId')->references('ItemVariantId')->on('TblMItemVariant');
            $table->foreign('OrderReturnId')->references('OrderReturnId')->on('TblTOrderReturn');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TblXItemOrderReturn', function (Blueprint $table) {
            $table->dropForeign('tblxitemorderreturn_itemvariantid_foreign');
            $table->dropForeign('tblxitemorderreturn_orderreturnid_foreign');
        });
    }
}
