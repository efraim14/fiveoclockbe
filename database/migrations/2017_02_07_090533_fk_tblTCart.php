<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkTblTCart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TblTCart', function (Blueprint $table) {
            $table->foreign('UserId')->references('UserId')->on('TblMUser');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TblTCart', function (Blueprint $table) {
            $table->dropForeign('tbltcart_userid_foreign');
        });
    }
}
