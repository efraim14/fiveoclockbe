<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkTblTOrderReturn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TblTOrderReturn', function (Blueprint $table) {
            $table->foreign('OrderId')->references('OrderId')->on('TblTOrder');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TblTOrderReturn', function (Blueprint $table) {
            $table->dropForeign('tbltorderreturn_orderid_foreign');
        });
    }
}
