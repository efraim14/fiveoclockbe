<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkTblXItemOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TblXItemOrder', function (Blueprint $table) {
            $table->foreign('ItemVariantId')->references('ItemVariantId')->on('TblMItemVariant');
            $table->foreign('OrderId')->references('OrderId')->on('TblTOrder');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TblXItemOrder', function (Blueprint $table) {
            $table->dropForeign('tblxitemorder_itemvariantid_foreign');
            $table->dropForeign('tblxitemorder_orderid_foreign');
        });
    }
}
