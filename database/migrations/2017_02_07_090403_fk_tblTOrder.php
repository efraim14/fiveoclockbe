<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkTblTOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TblTOrder', function (Blueprint $table) {
            
            $table->foreign('ProfileId')->references('ProfileId')->on('TblTProfile');
            $table->foreign('VoucherId')->references('VoucherId')->on('TblMVoucher');
            $table->foreign('CourierId')->references('CourierId')->on('TblMCourier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TblTOrder', function (Blueprint $table) {
            $table->dropForeign('tbltorder_profileid_foreign');
            $table->dropForeign('tbltorder_voucherid_foreign');
            $table->dropForeign('tbltorder_courierid_foreign');
        });
    }
}
