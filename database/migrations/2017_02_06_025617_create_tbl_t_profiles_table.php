<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblTProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TblTProfile', function (Blueprint $table) {
            $table->bigIncrements('ProfileId');
            $table->bigInteger('UserId')->unsigned();
            $table->bigInteger('SubdistrictId')->unsigned();
            $table->string('ProfileName');
            $table->string('Recipient');
            $table->string('Address');
            $table->string('Phone', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TblTProfile');
    }
}
