<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblMUser extends Model
{
    protected $table='TblMUser';
    protected $primaryKey='UserId';
    protected $fillable = ['RoleId', 'UserName', 'Password', 'IsActive'];

    public function profile() {
    	return $this->hasMany('App\TblTProfile','UserId','UserId');
    }

    public function token() {
    	return $this->hasMany('App\TblTToken','UserId','UserId');
    }

    public function role() {
    	return $this->belongsTo('App\TblMRole','RoleId','RoleId');
    }

    public function cart() {
        return $this->hasMany('App\TblTCart','UserId','UserId');
    }
}
