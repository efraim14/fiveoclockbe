<?php

namespace App\Dto;

class ResponseStatusDto{

	public $code;

	public $status;

	public $definition;
}