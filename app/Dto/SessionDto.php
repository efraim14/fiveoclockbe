<?php

namespace App\Dto;

class SessionDto{

	public $username;

	public $roleName;

	public $level;

	public $token;
}