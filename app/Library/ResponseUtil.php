<?php

namespace App\Library;

use App\Dto\ResponseStatusDto;
use App\Dto\ResponseDto;

class ResponseUtil{

	public static function generateResponse($statusCode, $data, $code, $status, $message){
		$responseStatusDto = new ResponseStatusDto();
		$responseStatusDto->status = $status;
		$responseStatusDto->code = $code;
		$responseStatusDto->definition = $message;
		$responseDto = new ResponseDto();
		$responseDto->responseStatusDto = $responseStatusDto;
		$responseDto->data = $data;
		return response()->json($responseDto, $statusCode);
	}
}