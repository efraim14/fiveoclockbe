<?php

namespace App\Library;

use App\TblTToken;
use JWTFactory;
use JWTAuth;

class TokenUtil{

	public static function generateToken($userid, $username){
		$valid = false;
		$token;
		while(!$valid){
			$payload = JWTFactory::sub($userid)->aud('user')->user(['username' => $username])->make();
			$token = JWTAuth::encode($payload);
			$checkToken = TblTToken::where('Token', $token)->first();
			if($checkToken === null){
				$valid = true;
			}
		}

    	return $token;
	}

	public static function isTokenExpire($token){
		$tokenDto = TblTToken::where('Token', $token)->first();
		//check apakah timenya lebih kurang dr skrg, jika ya maka return true, jika tidak maka return false.
		
	}
}