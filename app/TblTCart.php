<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblTCart extends Model
{
    protected $table='TblTCart';
    protected $primaryKey='CartId';
    protected $fillable = ['UserId', 'Notes'];

    public function user() {
    	return $this->belongsTo('App\TblMUser','UserId','UserId');
    }

    public function itemVariant() {
    	return $this->belongsToMany('App\TblMItemVariant','TblXItemCart','CartId','ItemVariantId');
    }
}
