<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblMItem extends Model
{
    protected $table='TblMItem';
    protected $primaryKey='ItemId';
    protected $fillable = ['BrandId', 'ItemName', 'Notes', 'IsActive'];

    public function category() {
        return $this->belongsToMany('App\TblMCategory', 'TblXCategoryItem', 'ItemId', 'CategoryId');
    }

    public function brand() {
        return $this->belongsTo('App\TblMBrand','BrandId','BrandId');
    }

    public function itemVariant() {
        return $this->hasMany('App\TblMItemVariant','ItemId','ItemId');
    }
}
