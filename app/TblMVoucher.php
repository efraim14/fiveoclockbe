<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblMVoucher extends Model
{
    protected $table='TblMVoucher';
    protected $primaryKey='VoucherId';
    protected $fillable = ['VoucherName', 'Limit', 'Stock', 'ExpirationDate', 'Notes', 'IsActive'];

    public function order() {
        return $this->hasMany('App\TblTOrder', 'VoucherId', 'VoucherId');
    }

    public function itemVariant() {
    	return $this->belongsToMany('App\TblMItemVariant', 'TblXItemVoucher', 'VoucherId', 'ItemVariantId');
    }
}