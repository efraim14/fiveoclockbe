<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblMBrand extends Model
{
    protected $table='TblMBrand';
    protected $primaryKey='BrandId';
    protected $fillable = ['BrandName', 'Notes', 'IsActive'];

    public function item() {
    	return $this->hasMany('App\TblMItem','BrandId','BrandId');
    }
}
