<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblTOrderReturn extends Model
{
    protected $table='TblTOrderReturn';
    protected $primaryKey='OrderReturnId';
    protected $fillable = ['OrderId', 'TimeStamp', 'Notes', 'IsActive'];

    public function itemVariant() {
    	return $this->belongsToMany('App\TblMItemVariant','TblXItemOrderReturn','OrderReturnId','ItemVariantId');
    }

    public function order() {
    	return $this->belongsTo('App\TblTOrder','OrderId','OrderId');
    }
}
