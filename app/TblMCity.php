<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblMCity extends Model
{
    protected $table='TblMCity';
    protected $primaryKey='CityId';
    protected $fillable = ['ProvinceId', 'CityName', 'Type'];

    public function province() {
    	return $this->belongsTo('App\TblMProvince','ProvinceId','ProvinceId');
    }

    public function subdistrict() {
    	return $this->hasMany('App\TblMSubdistrict','CityId','CityId');
    }
}
