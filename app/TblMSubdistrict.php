<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblMSubdistrict extends Model
{
    protected $table='TblMSubdistrict';
    protected $primaryKey='SubdistrictId';
    protected $fillable = ['CityId', 'SubdistrictName', 'ZipCode'];

    public function city() {
    	return $this->belongsTo('App\TblMCity','CityId','CityId');
    }

    public function profile() {
    	return $this->hasMany('App\TblMProfile','SubdistrictId','SubdistrictId');
    }
}
