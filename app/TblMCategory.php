<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblMCategory extends Model
{
    protected $table='TblMCategory';
    protected $primaryKey='CategoryId';
    protected $fillable = ['CategoryName', 'Notes', 'IsActive'];

    public function item() {
        return $this->belongsToMany('App\TblMItem', 'TblXCategoryItem', 'CategoryId', 'ItemId');
    }
}
