<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblMMenu extends Model
{
    protected $table='TblMMenu';
    protected $primaryKey='MenuId';
    protected $fillable = ['MenuName', 'Notes', 'IsActive'];

    public function role() {
    	return $this->belongsToMany('App\TblMRole','TblXMenuRole','MenuId','RoleId');
    }
}