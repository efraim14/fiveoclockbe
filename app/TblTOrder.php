<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblTOrder extends Model
{
    protected $table='TblTOrder';
    protected $primaryKey='OrderId';
    protected $fillable = ['ProfileId', 'VoucherId', 'CourierId', 'PaymentMethod', 'NoResi', 'Notes', 'Invoice', 'OrderDate'];

    public function profile() {
    	return $this->belongsTo('App\TblTProfile','ProfileId','ProfileId');
    }

    public function voucher() {
    	return $this->belongsTo('App\TblMVoucher','VoucherId','VoucherId');
    }

    public function orderLog() {
    	return $this->hasMany('App\TblTOrderLog','OrderId','OrderId');
    }

    public function courier() {
    	return $this->belongsTo('App\TblMCourier', 'CourierId','CourierId');
    }

    public function itemVariant() {
    	return $this->belongsToMany('App\TblMItemVariant', 'TblXItemOrder', 'OrderId', 'ItemVariantId');
    }

    public function orderReturn() {
    	return $this->hasMany('App\TblTOrderReturn','OrderId','OrderId');
    }

}
