<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblTOrderLog extends Model
{
    protected $table='TblTOrderLog';
    protected $primaryKey='OrderLogId';
    protected $fillable = ['OrderId', 'Status', 'TimeStamp'];

    public function order() {
    	return $this->belongsTo('App\TblTOrder','OrderId','OrderId');
    }
}
