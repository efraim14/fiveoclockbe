<?php

namespace App\BL;

use App;
use App\Dto\SessionDto;
use App\Library\TokenUtil;
use Carbon\Carbon;

class AuthBL{

	public function registerUser($registerDto){
		$username = strtolower($registerDto->username);

		//check dulu usernya sudah ada atau blm
		$checkUser = TblMUser::where('UserName', $username)
								->where('IsActive', '1')->first();

		if($checkUser === null){
			//masukin usernya ke DB
			$user = new TblMUser();
			$user->UserName = $username;
			$user->Password = Hash::make($registerDto->password);
			$user->IsActive = true;
			$user->roleId = 3;
			$user->save();

			//klo udah save, bikin session buat usernya + masukin active token ke dalam DB
			$sessionDto = new SessionDto();
			$sessionDto->username = $username;
			$token = TokenUtil::generateToken($user->UserId, $user->UserName);
			$sessionDto->token = $token;

			//masukin tokennya ke dalam DB
			$tblToken = new TblTToken();
			$tblToken->UserId = $user->UserId;
			$tblToken->token = $token;
			$tblToken->ExpiredTimeStamp = Carbon::now()->addDays(7);

			return $sessionDto;
		}else{
			throw new Exception('Username has been used, please use another username');
		}
	}

}

