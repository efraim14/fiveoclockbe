<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblMRole extends Model
{
    protected $table='TblMRole';
    protected $primaryKey='RoleId';
    protected $fillable = ['RoleName', 'Level', 'IsActive'];

    public function menu() {
    	return $this->belongsToMany('App\TblMMenu','TblXMenuRole','RoleId','MenuId');
    }

    public function user() {
    	return $this->hasMany('App\TblMUser','RoleId','RoleId');
    }
}
