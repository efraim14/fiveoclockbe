<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblTProfile extends Model
{
    protected $table='TblTProfile';
    protected $primaryKey='ProfileId';
    protected $fillable = ['UserId', 'SubdistrictId', 'ProfileName', 'Recipient','Address','Phone'];

    public function user() {
    	return $this->belongsTo('App\TblMUser','UserId','UserId');
    }

    public function subdistrict() {
    	return $this->belongsTo('App\TblMSubdistrict','SubdistrictId','SubdistrictId');
    }

    public function order() {
    	return $this->hasMany('App\TblTOrder','ProfileId','ProfileId');
    }
}
