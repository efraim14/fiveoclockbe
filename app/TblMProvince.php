<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblMProvince extends Model
{
    protected $table='TblMProvince';
    protected $primaryKey='ProvinceId';
    protected $fillable = ['ProvinceName'];

    public function city() {
    	return $this->hasMany('App\TblMProvince','ProvinceId','ProvinceId');
    }
}
