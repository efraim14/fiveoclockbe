<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblTToken extends Model
{
    protected $table='TblTToken';
    protected $primaryKey='TokenId';
    protected $fillable = ['UserId', 'Token', 'ExpiredTimeStamp'];

    public function user() {
    	return $this->belongsTo('App\TblMUser','UserId','UserId');
    }
}
