<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblMItemVariant extends Model
{
    protected $table='TblMItemVariant';
    protected $primaryKey='ItemVariantId';
    protected $fillable = ['ItemId', 'VariantName', 'BasePrice', 'ResellerPrice', 'Price', 'Discount', 'Weight', 'Color', 'IsActive'];

    public function voucher() {
    	return $this->belongsToMany('App\TblMVoucher', 'TblXItemVoucher', 'ItemVariantId', 'VoucherId');
    }

    public function order() {
    	return $this->belongsToMany('App\TblTOrder', 'TblXItemOrder', 'ItemVariantId', 'VoucherId');
    }

    public function orderReturn() {
    	return $this->belongsToMany('App\TblTOrderReturn','TblXItemOrderReturn','ItemVariantId','OrderReturnId');
    }

    public function cart() {
        return $this->belongsToMany('App\TblTCart','TblXItemCart','ItemVariantId','CartId');
    }

    public function item() {
    	return $this->belongsTo('App\TblMItem','ItemId','ItemId');
    }
}
