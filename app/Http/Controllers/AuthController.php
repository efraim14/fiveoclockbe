<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\ResponseUtil;
use App\BL\AuthBL;
use App\Dto\SessionDto;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    
	protected $authBL;

	public function __construct(AuthBL $authBL)
    {
        $this->authBL = $authBL;
    }

    public function register(Request $request){
    	$registerDto = json_decode($request->getContent(), true);

		$rules = array(
        'username' => 'required',
        'password' => 'required',
    	);

    	$validation = Validator::make($registerDto, $rules);

    	if ($validation->fails()) {
        	return ResponseUtil::generateResponse(200, null, 0, 'ERROR', 'Username must be filled');
    	}
    	//validation
    	if($registerDto->username === null)
    		return ResponseUtil::generateResponse(200, null, 0, 'ERROR', 'Username must be filled');
    	if($registerDto->password === null)
    		return ResponseUtil::generateResponse(200, null, 0, 'ERROR', 'Password must be filled');
    	// try{
    	// 	$SessionDto = $authBL->registerUser($registerDto);

    	// 	return ResponseUtil::generateResponse(200, $SessionDto, 1, 'SUCCESS', null);

    	// }catch (Exception $e){
    	// 	Log::error($e->getMessage());
    	// 	return ResponseUtil::generateResponse(200, null, 0, 'ERROR', $e->getMessage());
    	// }

    }

    public function login(Request $request){


    }
}
