<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblMCourier extends Model
{
    protected $table='TblMCourier';
    protected $primaryKey='CourierId';
    protected $fillable = ['CourierName', 'IsActive'];

    public function order() {
    	return $this->hasMany('App\TblTOrder','CourierId','CourierId');
    }
    
}
